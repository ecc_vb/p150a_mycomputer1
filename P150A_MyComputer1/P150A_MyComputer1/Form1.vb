﻿' Project:P150A_MyComputer1
' Auther:IE2A No.24, 村田直人
' Date: 2015年04月24日


Public Class Form1

    '配列の要素数を宣言
    Const Last As Integer = 9

    'ラベル配列の要素数をLastを使って宣言
    Dim ComLabels(Last) As Label

    'ラベルのコピーした回数をカウントする変数
    Dim Counter As Integer

    'ラベルのY座標の初期値を格納する変数
    Dim LabeIntTop As Integer

    'ラベルの配列間隔を格納する変数
    Dim CopyDistance As Integer



    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles Me.Load

        'ラベルY座標を変数に格納
        LabeIntTop = ComLabel.Location.Y

        'ラベルの配置間隔変数にラベルサイズ＋10を格納
        CopyDistance = ComLabel.Size.Width + 10

        'ラベルの参照をComLabelsの先頭の要素に代入
        ComLabels(Counter) = ComLabel

    End Sub


    Private Sub ComLabel_Click(ByVal sender As Object, e As EventArgs) Handles ComLabel.Click

        'ラベルを下げる
        With CType(sender, Label)
            .Location = New Point(.Location.X, .Location.Y + 10)
        End With

    End Sub

    'コピーボタン処理
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles CopyButton.Click

        'フォームのサイズ拡幅する
        Me.Size = New Size(Me.Size.Width + CopyDistance, Me.Size.Height)

        'カウンターのインクリメント
        Counter = Counter + 1

        '配列の先頭の要素に追加
        ComLabels(Counter) = New Label

        'コピーしたラベルのプロパティを設定
        With ComLabels(Counter)
            .Location = New Point(ComLabels(Counter - 1).Location.X + CopyDistance, LabeIntTop)
            .Size = ComLabel.Size
            .Image = ComLabel.Image
        End With

        'フォームにラベルを追加
        Me.Controls.Add(ComLabels(Counter))

        '新しいラベルのクリックイベントプロシージャに関連付けする
        AddHandler ComLabels(Counter).Click, AddressOf ComLabel_Click

        If Counter = Last Then

            'コピーボタン無効
            CopyButton.Enabled = False

        End If

    End Sub
End Class
